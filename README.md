# extract-email
> **A simple email extractor for obfuscated emails.**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4802268/extract-email.png)

[![version](https://img.shields.io/npm/v/extract-email.svg)](https://www.npmjs.org/package/extract-email)
[![downloads](https://img.shields.io/npm/dt/extract-email.svg)](https://www.npmjs.org/package/extract-email)
[![node](https://img.shields.io/node/v/extract-email.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/extract-email/badges/master/pipeline.svg)](https://gitlab.com/autokent/extract-email/pipelines)

## Installation
`npm install extract-email`

## Usage

### String Extract
```js
const ExtractEmail = require('extract-email');

let dotwords = [".","[dot]","-dot-"];
let atwords = ["@","[at]","-at-"];
let res = ExtractEmail.String("mehmet.kozan[at]live[dot]com",atwords,dotwords);
console.log(res[0].email);//mehmet.kozan@live.com
```

### Text File Extract (not implemented)
```js
const ExtractEmail = require('extract-email');

//not implemented
```

### CSV Extract (not implemented)
```js
const ExtractEmail = require('extract-email');

//not implemented
```

### Excel Extract (not implemented)
```js
const ExtractEmail = require('extract-email');

//not implemented
```

## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.