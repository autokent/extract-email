'use strict';
const fs = require('fs');
const reCache = new Map();


const atwordsArr = fs.readFileSync(__dirname + '/data/atwords.txt').toString().replace(/\r/g, '').split('\n');
const dotwordsArr = fs.readFileSync(__dirname + '/data/dotwords.txt').toString().replace(/\r/g, '').split('\n');

var matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;

const escapeStringRegexp = function (str) {
	if (typeof str !== 'string') {
		throw new TypeError('Expected a string');
	}

	return str.replace(matchOperatorsRe, '\\$&');
};

function string_extract(str, atwords, dotwords) {
	let mails = [];

	if (typeof atwords == "undefined" || typeof dotwords == "undefined") {
		atwords = atwordsArr;
		dotwords = dotwordsArr;
	}

	for (let atword of atwords) {
		if (atword == "") continue;
		for (let dotword of dotwords) {
			if (dotword == "") continue;
			const cacheKey = atword + dotword;
			let reMail = null;
			if (reCache.has(cacheKey)) {
				reMail = reCache.get(cacheKey);
			} else {
				let regexStr = `([a-z][a-z0-9.-]{1,25})([ ]?(${escapeStringRegexp(atword)})[ ]?)(([a-z][a-z0-9-]{1,25}([ ]?${escapeStringRegexp(dotword)}[ ]?)){1,3}[a-z]{2,4})`;
				reMail = new RegExp(regexStr, 'ig');
				reCache.set(cacheKey, reMail);
			}

			let matchArr = [];
			while ((matchArr = reMail.exec(str)) !== null) {
				mails.push({
					email: matchArr[0].replace(/[\s]+/g, "").replace(atword, "@").replace(dotword, "."),
					raw: matchArr[0]
				});
				//mails.push([matchArr[0].replace(/[\s]+/g,"").replace(atword,"@").replace(dotword,"."),matchArr[0]]);
			}

		}
	}
	return mails;
}

function formatted_text_extract(text) {
	let ret = {
		mailArr: [],
		msg: ""
	};
	let lineArr = text.split(/[\n\r]+/gm);
	for (let line of lineArr) {
		for (let data of line.split(/[\t,;]+/gm)) {

		}
	}

	return ret;
}

function text_file_extract(file_path) {
	let text = "";

	return formatted_text_extract(text);
}

function csv_file_extract(file_path) {
	let text = "";

	return formatted_text_extract(text);
}

function excel_file_extract(file_path) {
	let text = "";

	return formatted_text_extract(text);
}

module.exports.String = string_extract;
module.exports.TextFile = text_file_extract;
module.exports.CsvFile = csv_file_extract;
module.exports.ExcelFile = excel_file_extract;

//for testing purpose.
if (!module.parent) {
	let res = string_extract("mehmet.kozan [at] nanomagnetics-inst [dot] com", ["@", "[at]", "-at-"], [".", "[dot]", "-dot-"]);
	debugger;
}
