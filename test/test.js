const assert = require('assert');
let ExtractEmail = require('../');

describe('string test', function () {
	this.timeout(10000);

	it('should extract', function () {
		let dotwords = [".", "[dot]", "-dot-"];
		let atwords = ["@", "[at]", "-at-"];
		let res = ExtractEmail.String("mehmet.kozan [at] live [dot] com", atwords, dotwords);
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});

describe('string test with one space', function () {
	this.timeout(10000);

	it('should extract with one space', function () {
		let dotwords = [".", "[dot]", "-dot-"];
		let atwords = ["@", "[at]", "-at-"];
		let res = ExtractEmail.String("mehmet.kozan[at] live[dot] com", atwords, dotwords);
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});


describe('string test with no space', function () {
	this.timeout(10000);

	it('should extract with no space', function () {
		let dotwords = [".", "[dot]", "-dot-"];
		let atwords = ["@", "[at]", "-at-"];
		let res = ExtractEmail.String("mehmet.kozan[at]live[dot]com", atwords, dotwords);
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});



describe('string test with no params', function () {
	this.timeout(10000);

	it('should extract with no params-1', function () {
		let res = ExtractEmail.String("mehmet.kozan [at] live [dot] com");
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});

	it('should extract with no params-2', function () {
		let res = ExtractEmail.String("mehmet.kozan [at] live [dot] com");
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});

	it('should extract with no params-3', function () {
		ExtractEmail = require('../');
		
		let res = ExtractEmail.String("mehmet.kozan [at] live [dot] com");
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});

describe('string test with one space and no params', function () {
	this.timeout(10000);

	it('should extract with one space and no params', function () {
		let res = ExtractEmail.String("mehmet.kozan[at] live[dot] com");
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});


describe('string test with no space and no params', function () {
	this.timeout(10000);

	it('should extract with no space and no params', function () {
		let res = ExtractEmail.String("mehmet.kozan[at]live[dot]com");
		assert.equal(res[0].email, "mehmet.kozan@live.com");
	});
});



describe('text file test', function () {
	this.timeout(10000);

	it('should extract', function () {

	});
});

describe('csv file test', function () {
	this.timeout(10000);

	it('should extract', function () {

	});
});

describe('string test', function () {
	this.timeout(10000);

	it('should extract', function () {

	});
});

describe('string test', function () {
	this.timeout(10000);

	it('should extract', function () {

	});
});
